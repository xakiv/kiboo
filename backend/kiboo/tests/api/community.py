import pytest
from django.contrib.auth import get_user_model
from django.shortcuts import reverse
from rest_framework.test import APIClient, APITestCase

from kiboo.models import Community

User = get_user_model()


class KibooCommunityAPITestCase(APITestCase):
    fixtures = [
        "dump.json",
    ]

    def setUp(self):
        self.client = APIClient()

    @pytest.mark.django_db
    def test_community_list(self):
        response = self.client.get(reverse("kiboo:community-list"))
        self.assertEqual(response.status_code, 200, response.content)
        communities = Community.objects.all()
        self.assertEqual(len(response.json()), communities.count(), response.content)

    @pytest.mark.django_db
    def test_community_create_no_authent(self):
        data = {
            "name": "CommunityTest1",
        }
        self.client.force_authenticate(user=None)
        response = self.client.post(
            reverse("kiboo:community-list"), data=data, format="json"
        )
        self.assertEqual(response.status_code, 401, response.content)

    @pytest.mark.django_db
    def test_community_create_authent(self):
        data = {
            "name": "CommunityTest1",
        }
        self.client.force_authenticate(user=User.objects.first())
        response = self.client.post(
            reverse("kiboo:community-list"), data=data, format="json"
        )
        self.assertEqual(response.status_code, 201, response.content)

    @pytest.mark.django_db
    def test_community_update(self):
        data = {
            "name": "CommunityTest2",
        }
        creator = User.objects.first()
        self.client.force_authenticate(user=creator)
        response = self.client.post(
            reverse("kiboo:community-list"), data=data, format="json"
        )
        self.assertEqual(response.status_code, 201, response.content)

        community_id = response.json()["id"]
        data["name"] = "CommunityTestUpdate2"
        self.client.force_authenticate(user=creator)
        response = self.client.put(
            reverse("kiboo:community-detail", args=[community_id]),
            data=data,
            format="json",
        )
        self.assertEqual(response.status_code, 200, response.content)
        self.assertEqual(response.json()["name"], data["name"], response.content)

        response = self.client.get(
            reverse("kiboo:community-detail", args=[community_id]), format="json"
        )
        self.assertEqual(response.status_code, 200, response.content)
        self.assertEqual(response.json()["name"], data["name"], response.content)

    @pytest.mark.django_db
    def test_community_update_no_autent(self):
        data = {
            "name": "CommunityTest3",
        }
        creator = User.objects.first()
        self.client.force_authenticate(user=creator)
        response = self.client.post(
            reverse("kiboo:community-list"), data=data, format="json"
        )
        self.assertEqual(response.status_code, 201, response.content)

        community_id = response.json()["id"]
        data["name"] = "CommunityTestUpdate3"
        self.client.force_authenticate(user=None)
        response = self.client.put(
            reverse("kiboo:community-detail", args=[community_id]),
            data=data,
            format="json",
        )
        self.assertEqual(response.status_code, 401, response.content)
