import pytest
from django.contrib.auth import get_user_model
from django.shortcuts import reverse
from rest_framework.test import APIClient, APITestCase

User = get_user_model()


class KibooUserAPITestCase(APITestCase):
    fixtures = [
        "dump.json",
    ]

    def setUp(self):
        self.client = APIClient()

    @pytest.mark.django_db
    def test_user_list(self):
        response = self.client.get(reverse("kiboo:user-list"))
        self.assertEqual(response.status_code, 200, response.content)
        active_users = User.objects.filter(is_active=True)
        self.assertEqual(len(response.json()), active_users.count(), response.content)

    @pytest.mark.django_db
    def test_user_create_authent(self):
        data = {
            "first_name": "UserTest1",
            "last_name": "UserTest1",
            "email": "usertest1@email.com",
            "password": "passpass1",
            "confirm_password": "passpass1",
        }
        self.client.force_authenticate(user=None)
        response = self.client.post(
            reverse("kiboo:user-list"), data=data, format="json"
        )
        self.assertEqual(response.status_code, 201, response.content)
        data = {
            "email": "USERTEST1@email.COM",
            "password": "passpass1",
        }
        response = self.client.post(
            reverse("token_obtain_pair"), data=data, format="json"
        )
        self.assertEqual(response.status_code, 200, response.content)

    @pytest.mark.django_db
    def test_user_create_exists(self):
        data = {
            "first_name": "User2",
            "last_name": "User2",
            "email": "user2@email.com",
            "password": "passpass1",
            "confirm_password": "passpass1",
        }
        self.client.force_authenticate(user=None)
        response = self.client.post(
            reverse("kiboo:user-list"), data=data, format="json"
        )
        self.assertEqual(response.status_code, 400, response.content)
        self.assertEqual(
            response.json()["detail"]["email"][0],
            "user with this email address already exists.",
            response.content,
        )

    @pytest.mark.django_db
    def test_user_update(self):
        data = {
            "first_name": "UserTest1",
            "last_name": "UserTest1",
            "email": "usertest1@email.com",
            "password": "passpass1",
            "confirm_password": "passpass1",
        }
        self.client.force_authenticate(user=None)
        response = self.client.post(
            reverse("kiboo:user-list"), data=data, format="json"
        )
        self.assertEqual(response.status_code, 201, response.content)
        user_id = response.json()["id"]
        data["first_name"] = "UserTestUpdate1"
        data["last_name"] = "UserTestUpdate1"
        data["password"] = "passpass2"
        data["confirm_password"] = "passpass2"
        self.client.force_authenticate(user=User.objects.get(pk=user_id))
        response = self.client.put(
            reverse("kiboo:user-detail", args=[user_id]),
            data=data,
            format="json",
        )
        self.assertEqual(response.status_code, 200, response.content)
        self.assertEqual(
            response.json()["first_name"], "UserTestUpdate1", response.content
        )
        self.assertEqual(
            response.json()["last_name"], "UserTestUpdate1", response.content
        )

        data = {
            "email": "USERTEST1@email.COM",
            "password": "passpass2",
        }
        response = self.client.post(
            reverse("token_obtain_pair"), data=data, format="json"
        )
        self.assertEqual(response.status_code, 200, response.content)
