import pytest
from django.contrib.auth import get_user_model
from django.shortcuts import reverse
from rest_framework.test import APIClient, APITestCase

from kiboo.models import Community

User = get_user_model()


class KibooCommunitySubscriberAPITestCase(APITestCase):
    fixtures = [
        "dump.json",
    ]

    def setUp(self):
        self.client = APIClient()

    @pytest.mark.django_db
    def test_community_subscribe(self):
        data = {"name": "CommunityTest4"}
        creator = User.objects.first()
        self.client.force_authenticate(user=creator)
        response = self.client.post(
            reverse("kiboo:community-list"), data=data, format="json"
        )
        self.assertEqual(response.status_code, 201, response.content)

        community_id = response.json()["id"]
        user2 = User.objects.get(email="user2@email.com")
        self.client.force_authenticate(user=user2)
        data = {"community": community_id}
        response = self.client.post(
            reverse("kiboo:communitysubscriber-list"),
            data=data,
            format="json",
        )
        self.assertEqual(response.status_code, 201, response.content)

        community = Community.objects.get(pk=community_id)
        subscription = community.communitysubscriber_set.first()
        self.assertEqual(
            subscription.user.email, "user2@email.com", subscription.__dict__
        )
        self.assertIsNotNone(subscription.requested_at, subscription.__dict__)

        user3 = User.objects.get(email="user3@email.com")
        self.client.force_authenticate(user=user3)
        data = {"community": community_id}
        response = self.client.post(
            reverse("kiboo:communitysubscriber-list"),
            data=data,
            format="json",
        )
        self.assertEqual(response.status_code, 201, response.content)
        subscription = community.communitysubscriber_set.last()
        self.assertEqual(community.subscribers.count(), 2, community.subscribers.all())
        self.assertEqual(
            subscription.user.email, "user3@email.com", community.subscribers.all()
        )
        self.assertIsNotNone(subscription.requested_at, subscription.__dict__)

    @pytest.mark.django_db
    def test_community_multi_subscribe(self):
        data = {"name": "CommunityTest5"}
        creator = User.objects.first()
        self.client.force_authenticate(user=creator)
        response = self.client.post(
            reverse("kiboo:community-list"), data=data, format="json"
        )
        self.assertEqual(response.status_code, 201, response.content)

        community_id = response.json()["id"]
        user4 = User.objects.get(email="user4@email.com")
        self.client.force_authenticate(user=user4)
        data = {"community": community_id}
        response = self.client.post(
            reverse("kiboo:communitysubscriber-list"),
            data=data,
            format="json",
        )
        self.assertEqual(response.status_code, 201, response.content)

        community_5 = Community.objects.get(pk=community_id)

        data = {"name": "CommunityTest6"}
        creator = User.objects.first()
        self.client.force_authenticate(user=creator)
        response = self.client.post(
            reverse("kiboo:community-list"), data=data, format="json"
        )
        self.assertEqual(response.status_code, 201, response.content)

        community_id = response.json()["id"]
        data = {"community": community_id}
        community_6 = Community.objects.get(pk=community_id)
        self.client.force_authenticate(user=user4)
        response = self.client.post(
            reverse("kiboo:communitysubscriber-list"),
            data=data,
            format="json",
        )

        self.assertEqual(
            user4.subscribed_communities.count(),
            2,
            user4.subscribed_communities.all(),
        )
        self.assertTrue(
            user4.subscribed_communities.filter(name=community_5.name).exists(),
            user4.subscribed_communities.all(),
        )
        self.assertTrue(
            user4.subscribed_communities.filter(name=community_6.name).exists(),
            user4.subscribed_communities.all(),
        )

        self.client.force_authenticate(user=user4)
        response = self.client.get(
            reverse("kiboo:communitysubscriber-list"),
            data={"q": "pending"},
            format="json",
        )
        subscription1_id = response.json()[0]["id"]
        subscription2_id = response.json()[1]["id"]
        self.client.force_authenticate(user=user4)
        response = self.client.delete(
            reverse("kiboo:communitysubscriber-detail", args=[subscription1_id]),
            format="json",
        )
        self.assertEqual(response.status_code, 204, response.content)

        self.client.force_authenticate(user=creator)
        response = self.client.delete(
            reverse("kiboo:communitysubscriber-detail", args=[subscription2_id]),
            format="json",
        )
        self.assertEqual(response.status_code, 204, response.content)
        self.assertEqual(
            user4.subscribed_communities.count(),
            0,
            user4.subscribed_communities.all(),
        )

    @pytest.mark.django_db
    def test_community_subscribe_confirm(self):
        data = {"name": "CommunityTest5"}
        creator = User.objects.first()
        self.client.force_authenticate(user=creator)
        response = self.client.post(
            reverse("kiboo:community-list"), data=data, format="json"
        )
        self.assertEqual(response.status_code, 201, response.content)

        community_id = response.json()["id"]
        user2 = User.objects.get(email="user2@email.com")
        self.client.force_authenticate(user=user2)
        data = {"community": community_id}
        response = self.client.post(
            reverse("kiboo:communitysubscriber-list"),
            data=data,
            format="json",
        )
        self.assertEqual(response.status_code, 201, response.content)
        subscription_id = response.json()["id"]
        self.client.force_authenticate(user=creator)
        response = self.client.put(
            reverse("kiboo:communitysubscriber-detail", args=[subscription_id]),
            format="json",
        )
        self.assertEqual(response.status_code, 200, response.content)
        self.client.force_authenticate(user=creator)
        response = self.client.patch(
            reverse("kiboo:communitysubscriber-detail", args=[subscription_id]),
            format="json",
        )
        self.assertEqual(response.status_code, 200, response.content)

        self.client.force_authenticate(user=user2)
        response = self.client.put(
            reverse("kiboo:communitysubscriber-detail", args=[subscription_id]),
            format="json",
        )
        self.assertEqual(response.status_code, 403, response.content)
