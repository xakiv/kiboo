from django.core.exceptions import ValidationError as DjangoValidationError
from rest_framework import exceptions as drf_exceptions
from rest_framework.serializers import as_serializer_error
from rest_framework.views import exception_handler as drf_exception_handler

from kiboo import logger  # noqa: F401


def extended_exception_handler(exc, context):
    # Handle Django validation errors with DRF error serialization
    if isinstance(exc, DjangoValidationError):
        exc = drf_exceptions.ValidationError(as_serializer_error(exc))
    response = drf_exception_handler(exc, context)
    if response is None:
        return response
    detail = getattr(exc, "detail", None)
    if isinstance(detail, (list, dict)):
        response.data = {"detail": response.data}
    return response
