from django.contrib.auth import get_user_model
from rest_framework import serializers

from kiboo.models import Community

User = get_user_model()


class CommunitySerializer(serializers.ModelSerializer):
    class Meta:
        model = Community
        fields = ["id", "name"]

    def set_creator_as_moderator(self, instance):
        request = self.context["request"]
        user = request.user
        instance.moderators.add(user)

    def create(self, validated_data):
        try:
            community = Community.objects.create(**validated_data)
            self.set_creator_as_moderator(community)
        except Exception as err:
            raise serializers.ValidationError({"error": str(err)})
        return community

    def update(self, instance, validated_data):
        try:
            for field, value in validated_data.items():
                if value:
                    setattr(instance, field, value)
            instance.save()
        except Exception as err:
            raise serializers.ValidationError({"error": str(err)})
        instance.refresh_from_db()
        return instance
