from django.contrib.auth import get_user_model
from django.utils import timezone
from rest_framework import serializers

from kiboo.models import CommunitySubscriber

User = get_user_model()


class CommunitySubscriberSerializer(serializers.ModelSerializer):
    class Meta:
        model = CommunitySubscriber
        fields = ["id", "community", "user", "requested_at", "confirmed_at"]
        read_only_fields = ["user", "requested_at", "confirmed_at"]

    def create(self, validated_data):
        community = validated_data.get("community")
        user = self.context["request"].user
        community_subscriber, _created = community.add_subscriber(user)
        return community_subscriber

    def update(self, instance, validated_data):
        if instance.confirmed_at:
            instance.confirmed_at = None
        else:
            instance.confirmed_at = timezone.now()
        instance.save()
        return instance
