from django.contrib.auth import get_user_model
from rest_framework import serializers

from kiboo.api import messages

User = get_user_model()


class UserSerializer(serializers.ModelSerializer):
    password = serializers.CharField(write_only=True, max_length=128)
    confirm_password = serializers.CharField(write_only=True, max_length=128)

    class Meta:
        model = User
        fields = [
            "id",
            "first_name",
            "last_name",
            "email",
            "password",
            "confirm_password",
        ]

    def validate(self, attrs):
        password = attrs.get("password", None)
        confirm_password = attrs.pop("confirm_password", None)
        if password and password != confirm_password:
            raise serializers.ValidationError(
                {
                    "password": messages.PASSWORD_MISMATCH_ERROR,
                    "confirm_password": messages.PASSWORD_MISMATCH_ERROR,
                }
            )
        return attrs

    def validate_email(self, data):
        if isinstance(data, str):
            data = data.lower()
        return data

    def create(self, validated_data):
        try:
            user = User.objects.create_user(**validated_data)
        except Exception as err:
            raise serializers.ValidationError({"error": str(err)})
        return user


class UserUpdateSerializer(UserSerializer):
    password = serializers.CharField(required=False, write_only=True, max_length=128)

    confirm_password = serializers.CharField(
        required=False, write_only=True, max_length=128
    )

    def updated_email(self, instance, validated_data):
        email = validated_data.pop("email", None)
        if email and email != instance.email:
            instance.email = email
        return instance

    def updated_password(self, instance, validated_data):
        new_password = validated_data.pop("password", None)
        if new_password:
            instance.set_password(new_password)
        return instance

    def update(self, instance, validated_data):
        instance = self.updated_email(instance, validated_data)
        instance = self.updated_password(instance, validated_data)
        try:
            for field, value in validated_data.items():
                if value:
                    setattr(instance, field, value)
            instance.save()
        except Exception as err:
            raise serializers.ValidationError({"error": str(err)})
        instance.refresh_from_db()
        return instance
