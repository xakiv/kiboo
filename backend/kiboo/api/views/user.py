from django.contrib.auth import get_user_model
from rest_framework import decorators, mixins, permissions, viewsets
from rest_framework.response import Response

from kiboo.api.serializers.user import UserSerializer, UserUpdateSerializer

User = get_user_model()


class IsOwnerOrReadOnly(permissions.BasePermission):
    def has_object_permission(self, request, view, obj):
        if request.method in permissions.SAFE_METHODS:
            return True
        return obj.pk == request.user.pk


class UserView(
    mixins.CreateModelMixin,
    mixins.RetrieveModelMixin,
    mixins.ListModelMixin,
    mixins.UpdateModelMixin,
    viewsets.GenericViewSet,
):
    permission_classes = [IsOwnerOrReadOnly]

    serializer_class = UserSerializer

    queryset = User.objects.filter(is_active=True)

    def get_serializer_class(self):
        serializer = UserSerializer
        if self.request.method in ["PUT", "PATCH"]:
            serializer = UserUpdateSerializer
        return serializer

    def get_queryset(self):
        queryset = super().get_queryset()
        query = self.request.query_params.get("q", None)
        if query and query == "me":
            if not self.request.user.is_authenticated:
                queryset = queryset.none()
            else:
                queryset = queryset.filter(pk=self.request.user.pk)
        return queryset

    @decorators.action(detail=False, permission_classes=[permissions.IsAuthenticated])
    def me(self, request):
        serializer = self.get_serializer(request.user, many=False)
        return Response(serializer.data)
