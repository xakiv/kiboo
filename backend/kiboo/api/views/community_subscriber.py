from django.contrib.auth import get_user_model
from django.db.models import Q
from rest_framework import mixins, permissions, viewsets

from kiboo.api.serializers.community_subscriber import CommunitySubscriberSerializer
from kiboo.models import CommunitySubscriber

User = get_user_model()


class IsModeratorOrOwner(permissions.BasePermission):
    def has_permission(self, request, view):
        return request.user.is_authenticated

    def has_object_permission(self, request, view, obj):
        if not request.user.is_authenticated:
            return False
        if request.method == "DELETE":
            # Moderator and Owner can delete
            return obj.user.pk == request.user.pk or obj.community.is_moderator(
                request.user
            )
        if request.method in ["PUT", "PATCH"]:
            # Moderator can update (confirm subscription)
            return obj.community.is_moderator(request.user)


class CommunitySubscriberView(
    mixins.CreateModelMixin,
    mixins.RetrieveModelMixin,
    mixins.ListModelMixin,
    mixins.UpdateModelMixin,
    mixins.DestroyModelMixin,
    viewsets.GenericViewSet,
):
    permission_classes = [IsModeratorOrOwner]

    serializer_class = CommunitySubscriberSerializer

    queryset = CommunitySubscriber.objects.all()

    def get_queryset(self):
        # list subscription for moderated communities and subscribed communities
        queryset = (
            super()
            .get_queryset()
            .filter(
                Q(community__moderators=self.request.user) | Q(user=self.request.user)
            )
        )
        query = self.request.query_params.get("q", None)
        community_id = self.request.query_params.get("community_id", None)
        if community_id:
            queryset = queryset.filter(community__pk=community_id)
        if query:
            if query == "pending":
                queryset = queryset.filter(confirmed_at=None)
            if query == "confirmed":
                queryset = queryset.exclude(confirmed_at=None)
        return queryset

    def update(self, request, *args, **kwargs):
        kwargs["partial"] = True
        return super().update(request, *args, **kwargs)
