from django.contrib.auth import get_user_model
from rest_framework import mixins, permissions, viewsets

from kiboo.api.serializers.community import CommunitySerializer
from kiboo.models import Community

User = get_user_model()


class IsModeratorOrReadOnly(permissions.BasePermission):
    def has_permission(self, request, view):
        if request.method in permissions.SAFE_METHODS:
            return True
        return request.user.is_authenticated

    def has_object_permission(self, request, view, obj):
        if request.method in permissions.SAFE_METHODS:
            return True
        return obj.moderators.filter(pk=request.user.pk).exists()


class CommunityView(
    mixins.CreateModelMixin,
    mixins.RetrieveModelMixin,
    mixins.ListModelMixin,
    mixins.UpdateModelMixin,
    viewsets.GenericViewSet,
):
    permission_classes = [IsModeratorOrReadOnly]

    serializer_class = CommunitySerializer

    queryset = Community.objects.all()

    def get_queryset(self):
        queryset = super().get_queryset()
        query = self.request.query_params.get("q", None)
        if query and not self.request.user.is_authenticated:
            queryset = queryset.none()
        elif query and query == "moderated":
            queryset = queryset.filter(moderators=self.request.user.pk)
        elif query and query == "subscribed":
            queryset = queryset.filter(subscribers=self.request.user.pk)
        return queryset
