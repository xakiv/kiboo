from django.contrib import admin

from kiboo.models import Comment, Post, Thread

admin.site.register(Post)
admin.site.register(Comment)
admin.site.register(Thread)
