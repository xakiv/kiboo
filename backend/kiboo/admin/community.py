from django.contrib import admin

from kiboo.models import Community, CommunityModerator, CommunitySubscriber

admin.site.register(Community)
admin.site.register(CommunityModerator)
admin.site.register(CommunitySubscriber)
