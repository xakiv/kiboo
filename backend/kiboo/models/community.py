from django.conf import settings
from django.db import models
from django.utils import timezone


class CommunityUser(models.Model):
    community = models.ForeignKey(
        "kiboo.Community",
        on_delete=models.CASCADE,
    )

    user = models.ForeignKey(
        settings.AUTH_USER_MODEL, on_delete=models.SET_NULL, blank=True, null=True
    )

    class Meta:
        abstract = True


class CommunityModerator(CommunityUser):
    pass


class CommunitySubscriber(CommunityUser):
    requested_at = models.DateTimeField("requested at", null=True, blank=True)
    confirmed_at = models.DateTimeField("confirmed at", null=True, blank=True)

    class Meta:
        unique_together = [["community", "user"]]

    def save(self, *args, **kwargs):
        if not self.id:
            self.requested_at = timezone.now()
        super().save(*args, **kwargs)


class Community(models.Model):
    name = models.TextField()

    moderators = models.ManyToManyField(
        settings.AUTH_USER_MODEL,
        through="kiboo.CommunityModerator",
        related_name="moderated_communities",
    )

    subscribers = models.ManyToManyField(
        settings.AUTH_USER_MODEL,
        through="kiboo.CommunitySubscriber",
        related_name="subscribed_communities",
    )

    class Meta:
        verbose_name = "community"
        verbose_name_plural = "communities"

    def __str__(self):
        return self.name

    def add_subscriber(self, user):
        return self.communitysubscriber_set.get_or_create(user=user)

    def is_moderator(self, user):
        return user.is_authenticated and self.moderators.filter(pk=user.pk).exists()
