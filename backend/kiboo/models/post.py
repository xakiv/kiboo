from django.conf import settings
from django.db import models


class Comment(models.Model):
    body = models.TextField(blank=True)

    published_by = models.ForeignKey(
        settings.AUTH_USER_MODEL, on_delete=models.SET_NULL, null=True
    )

    published_at = models.DateTimeField("published at", null=True, blank=True)

    archived_at = models.DateTimeField("archived at", null=True, blank=True)


class Thread(models.Model):
    post = models.ForeignKey("kiboo.Post", on_delete=models.CASCADE)

    comment = models.ForeignKey("kiboo.Comment", on_delete=models.CASCADE)


class Post(models.Model):
    title = models.CharField(blank=True, max_length=2048)

    body = models.TextField(blank=True)

    link = models.URLField(blank=True, max_length=200)

    community = models.ForeignKey("kiboo.Community", on_delete=models.CASCADE)

    author = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)

    published_at = models.DateTimeField("published at", null=True, blank=True)

    archived_at = models.DateTimeField("archived at", null=True, blank=True)

    comments = models.ManyToManyField("kiboo.Comment", through="Thread")

    class Meta:
        verbose_name = "post"
        verbose_name_plural = "posts"

    def __str__(self):
        return self.title
