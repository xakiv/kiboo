from django.urls import include, path
from rest_framework.routers import DefaultRouter

from kiboo.api.views.community import CommunityView
from kiboo.api.views.community_subscriber import CommunitySubscriberView
from kiboo.api.views.user import UserView

app_name = "kiboo"

router = DefaultRouter()
router.register("users", UserView)
router.register("community-subscriber", CommunitySubscriberView)
router.register("communities", CommunityView)
urlpatterns = [
    path("api/", include(router.urls)),
]
