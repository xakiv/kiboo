from django.apps import AppConfig


class KibooConfig(AppConfig):
    name = "kiboo"
