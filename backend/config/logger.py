import logging

from django.core.management.color import color_style


class DjangoColorsFormatter(logging.Formatter):
    def __init__(self, *args, **kwargs):
        super(DjangoColorsFormatter, self).__init__(*args, **kwargs)
        self.style = self.configure_style(color_style())

    def configure_style(self, style):
        style.DEBUG = style.HTTP_NOT_MODIFIED
        style.INFO = style.HTTP_INFO
        style.WARNING = style.HTTP_NOT_FOUND
        style.ERROR = style.ERROR
        style.CRITICAL = style.HTTP_SERVER_ERROR
        return style

    def format(self, record):
        message = logging.Formatter.format(self, record)
        colorizer = getattr(self.style, record.levelname, self.style.HTTP_SUCCESS)
        return colorizer(message)


VERBOSE_FORMAT = "{levelname} {name} {asctime} {pathname}:{lineno} \n{message} \n"

COLORED_FORMAT = "{levelname} {name} {asctime} \n{pathname}:{lineno} \n{message} \n"

LOGGING = {
    "version": 1,
    "disable_existing_loggers": False,
    "handlers": {
        "console": {
            "level": "DEBUG",
            "class": "logging.StreamHandler",
            "formatter": "colored",
        },
    },
    "formatters": {
        "verbose": {
            "format": VERBOSE_FORMAT,
            "style": "{",
        },
        "colored": {
            "()": DjangoColorsFormatter,
            "format": COLORED_FORMAT,
            "style": "{",
        },
    },
    "loggers": {
        "django.request": {
            "handlers": ["console"],
            "level": "ERROR",
            "propagate": False,
        },
        "kiboo": {
            "handlers": ["console"],
            "level": "DEBUG",
            "propagate": False,
        },
    },
}
