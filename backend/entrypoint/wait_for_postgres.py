#!/usr/bin/env python

import socket
import time

import environ

env = environ.Env()
env.read_env()

port = env.int("POSTGRES_PORT", default=5432)
host = env("POSTGRES_HOST", default="postgres")

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
while True:
    try:
        s.connect((host, port))
        s.close()
        break
    except socket.error as _err:
        print("WAITING FOR POSTGRES")
        time.sleep(0.1)
