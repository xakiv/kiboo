#!/bin/bash

python /code/entrypoint/wait_for_postgres.py

python manage.py collectstatic --no-input
python manage.py showmigrations

if [ "$DOCKER_MIGRATE" == "True" ]; then
  python manage.py makemigrations --no-input
  python manage.py migrate
fi
if [ "$DOCKER_INIT_SUPERUSER" == "True" ]; then
  python manage.py createsuperuser --no-input
fi

gunicorn config.wsgi --bind 0.0.0.0:8080 --workers=3 --access-logfile - --reload --timeout 600
