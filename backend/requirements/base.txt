# Django & co
# ------------------------------------------------------------------------------
django>=4.2,<4.3  # https://github.com/django/django
django-environ==0.11.2  # https://github.com/joke2k/django-environ
djangorestframework==3.14.0  # https://github.com/encode/django-rest-framework
djangorestframework-simplejwt[crypto]==5.3.0  # https://github.com/jazzband/djangorestframework-simplejwt
django-cors-headers==3.14.0  # https://github.com/adamchainz/django-cors-headers
