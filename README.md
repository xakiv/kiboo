# Link aggregator POC

## Install

- Clone sources
```
git clone https://gitlab.com/xakiv/kiboo.git
```

- Build services with docker compose
```
cd kiboo/
docker-compose up --build
```
- Django admin and DRF browser dev url are available at:
```
http://localhost:8000/admin
http://localhost:8000/api
```
- VueJS client is available at:
```
http://localhost:5173/
```
- Run api test cases (pytest)
```
docker exec -it kiboo-backend-1 pytest
```


## Status
⚠️ WIP ⚠️
